import requests
from requests.auth import HTTPBasicAuth
import json

# Rest API call to JSM, gets source field value from issue
def getIssueValue(issue, sourceField):
    # Refer to Jira API for setting up API calls
    # https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-issues/#api-group-issues
    url = "https://crateandbarrel.atlassian.net/rest/api/3/issue/"+issue

    # Use service account for Rest API calls
    auth = HTTPBasicAuth("abc@crateandbarrel.com", "<api_key>")
    headers = {"Accept": "application/json"}
    response = requests.request("GET", url, headers=headers, auth=auth)
    issue = json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": "))
    issue = json.loads(issue)

    # Rest API call returns JSON dictionary of all fields in issue
    value = issue['fields'][sourceField]
    return value


def editJSM(issue, sourceField, targetField, value):
    # Refer to Jira API for setting up API calls
    # https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-issues/#api-group-issues
    url = "https://crateandbarrel.atlassian.net/rest/api/3/issue/"+issue

    # Use service account for Rest API calls
    auth = HTTPBasicAuth("abc@crateandbarrel.com", "<api_key>")
    headers = {"Accept": "application/json", "Content-Type": "application/json"}

    payload = json.dumps( {
        "fields": {
            str(targetField): str(value),
        }
    })
    response = requests.request("PUT", url, data=payload, headers=headers, auth=auth)
    return response

# Get current value of issue from source field, the copy value into the new target field
def updateJSM(issue, sourceField, targetField):
    # Get current issue sourceField value
    value = getIssueValue(issue, sourceField)
    if(value == None or value == ''):
        return 'skipped'

    # Copy source field into targetfield for issue
    return editJSM(issue, sourceField, targetField, value)


# Copies the value from an issue's source field to the target field
# Used to migrate current value into a new custom field
# Example: customfield_X accepts only integers, but the form needs a new custom field that accepts all strings
#          so you must copy all existing values in current issues into the new custom field
def main():
    # Array of Issues that need bulk editing
    issues = ['ISMP-XXXX', 'ISMP-YYYY', 'ISMP-ZZZZ']    # example: ISMP-1234
    sourceField = 'customfield_X' # Custom Field ID for source field, example: customfield_11694
    targetField = 'customfield_Y' # Custom Field ID for target field, example: customfield_11700

    for issue in issues:
        print(issue)
        print(updateJSM(issue, sourceField, targetField))


if __name__ == '__main__':
    main()
